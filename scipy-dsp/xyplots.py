#!/usr/bin/python3.4
#


import math
import time

import numpy # as np

import matplotlib.pyplot # as plt

PI = math.pi
TWO_PI = 2 * math.pi
TWO_PI_J = 2j * math.pi

#---------------------------------------------------------------------#

# https://en.wikipedia.org/wiki/Lissajous_curve
N = 80
t = numpy.arange(-N//2, N//2)
x = 3 * numpy.sin(TWO_PI * (3 * t / N + 1/2))
y = 2 * numpy.sin(TWO_PI * 2 * t / N)
fig = matplotlib.pyplot.figure('lisajous')
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, y, 'ro-')
ax.set_aspect('equal')

# x = rand; y = rand
N = 5000
x = numpy.random.normal(0, 2, N)
y = numpy.random.uniform(-2, 2, N)
fig = matplotlib.pyplot.figure('random')
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, y, 'r.')
ax.set_xlim(-5, 5)
ax.set_ylim(-3, 3)
ax.set_aspect('equal')

# x-y plot i-q 1a + 2b
N = 80
x = (1 * numpy.cos(TWO_PI * 1 * t / N)
     + 0.3 * numpy.cos(TWO_PI * 7 * t / N))
y = (1 * numpy.sin(TWO_PI * 1 * t / N)
     + 0.3 * numpy.sin(TWO_PI * 7 * t / N))
fig = matplotlib.pyplot.figure('I-Q plot')
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, y, 'ro-')
ax.set_aspect('equal')

matplotlib.pyplot.show()

#--#
