#!/usr/bin/python3.4
# delta <--> sinc

import math
import time

import numpy # as np

import matplotlib.pyplot # as plt

BG = 'lightgray'
FG = 'green'

PI = math.pi
TWO_PI = 2 * math.pi
TWO_PI_J = 2j * math.pi

C = 2**(1/2) / 2

N1 = 16

ticks = numpy.arange(-N1//2, N1//2)

x1 = numpy.array([ -8, -7, -6, -5, -4, -3, -2, -1,
                   0, +1, +2, +3, +4, +5, +6, +7 ])


F1 = 4
y1 = numpy.array([ +1, 0, -1, 0, +1, 0, -1, 0,
                   +1, 0, -1, 0, +1, 0, -1, 0])

F2 = 8
y2 = numpy.array([ +1, -1, +1, -1, +1, -1, +1, -1,
                   +1, -1, +1, -1, +1, -1, +1, -1])

F3 = 4
y3 = numpy.array([ +C, +C, -C, -C, +C, +C, -C, -C,
                   +C, +C, -C, -C, +C, +C, -C, -C])

N2 = 800
x2 = (numpy.arange(N2) - N2//2) / (N2 / N1)
z1 = numpy.cos(F1 * TWO_PI * x2/N1)
z2 = numpy.cos(F2 * TWO_PI * x2/N1)
z3 = numpy.cos(F3 * TWO_PI * (x2/N1 - 0.5/N1))

if True:
    fig = matplotlib.pyplot.figure('frobtastic')
    NP = 3

    ax = fig.add_subplot(NP, 1, 1)
    ax.set_title('Sinusoidal waves???')
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(x2, z1, BG, linewidth=6)
    ax.plot(x1, y1, 'bo-')
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-2, 2))

    ax = fig.add_subplot(NP, 1, 2)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(x2, z2, BG, linewidth=6)
    ax.plot(x1, y2, 'bo-')
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-2, 2))

    ax = fig.add_subplot(NP, 1, 3)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(x2, z3, BG, linewidth=6)
    ax.plot(x1, y3, 'bo-')
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-2, 2))

def delta(k):
    y = numpy.zeros_like(x1)
    y[x1 == k] = 1
    return y

def sinc(k):
    return numpy.sinc(x2 - k)

#http://matplotlib.org/users/mathtext.html#mathtext-tutorial

if True:
    fig = matplotlib.pyplot.figure('foo')
    NP = 1

    ax = fig.add_subplot(NP, 1, 1)
    ax.set_title(r'delta $\leftrightarrow$ sinc', fontsize=32)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')

    ax.stem(x1, delta(0), 'b')
    ax.plot(x2, sinc(0), 'b-', label=r'$x_0 = 0$')

    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-0.5, 1.2))
    #ax.legend()


if True:
    fig = matplotlib.pyplot.figure('bar')
    NP = 2

    ax = fig.add_subplot(NP, 1, 1) # XXX chaneg title here:
    ax.set_title(r'delta $\leftrightarrow$ sinc', fontsize=32)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')

    ax.stem(x1, delta(0), 'b')
    ax.plot(x2, sinc(0), 'b-', label=r'$x_0 = 0$')

    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-2, 2))
    ax.legend()

    ax = fig.add_subplot(NP, 1, 2)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')

    ax.stem(x1, 0.5 * delta(1), 'r')
    ax.plot(x2, 0.5 * sinc(1), 'r-', label=r'$x_0 = +1$, A=1/2')

    ax.stem(x1, -delta(-2), 'g-')
    ax.plot(x2, -sinc(-2), 'g-', label=r'$x_0 = -2$, A=-1')

    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-2, 2))
    ax.legend()

if False:
    ax = fig.add_subplot(NP, 1, 3)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(x2, z2, 'lightblue', linewidth=6)
    for k, a in zip(x1, y2):
        ax.plot(x2, a * sinc(k), 'r-')
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-2, 2))


if True:
    fig = matplotlib.pyplot.figure('baz')
    NP = 3

    ax = fig.add_subplot(NP, 1, 1)
    ax.set_title('Sum of sincs')
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(x2, z1, BG, linewidth=6)
    y = numpy.zeros(N2)
    for k, a in zip(x1, y1):
        y += a * sinc(k)
    ax.plot(x2, y)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-2, 2))

    ax = fig.add_subplot(NP, 1, 2)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(x2, z2, BG, linewidth=6)
    y = numpy.zeros(N2)
    for k, a in zip(x1, y2):
        y += a * sinc(k)
    ax.plot(x2, y)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-2, 2))


    ax = fig.add_subplot(NP, 1, 3)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(x2, z3, BG, linewidth=6)
    y = numpy.zeros(N2)
    for k, a in zip(x1, y3):
        y += a * sinc(k)
    ax.plot(x2, y)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.grid(which='major')
    ax.set_ylim((-2, 2))

# Plot: sum of all N cos ==> sinc


matplotlib.pyplot.show()

#--#
