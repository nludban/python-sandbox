#!/usr/bin/python3.4
#
# Adapted from:
#	matplotlib.org/examples/pylab_examples/pythonic_matplotlib.html
#

import math
import time

import numpy # as np

import matplotlib.pyplot # as plt

PI = math.pi
TWO_PI = 2 * math.pi
TWO_PI_J = 2j * math.pi

#---------------------------------------------------------------------#

def profile(f, *args, **kwargs):
    t0 = time.time()
    rv = f(*args, **kwargs)
    dt = time.time() - t0
    print('%r: %.6f seconds' % ( f, dt ))
    return rv

#---------------------------------------------------------------------#

def list_data(N):
    x = [ ]
    y = [ ]
    for k in range(-N//2, N//2):
        phi = 3 * k / N
        x.append(k)
        y.append(math.cos(2 * math.pi * phi))
    return x, y

def numpy_data(N):
    if False:
        # 10k -> 6.2ms
        k = numpy.arange(N) - N//2
        phi = 3 * k / N
        y = numpy.cos(2 * math.pi * phi)
    else:
        # 10k -> 5.1ms
        k = numpy.arange(-N//2, N//2)
        phi = k * (2 * math.pi * 3 / N)
        y = numpy.cos(phi)
    return k, y

N = 80 #10000
x1, y1 = profile(list_data, N)
x2, y2 = profile(numpy_data, N)

if True:
    fig = matplotlib.pyplot.figure(1)

    ax = fig.add_subplot(2, 1, 1)
    ax.set_title('Sinusoidal waves')
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(x1, y1, 'ro-')
    ax.grid(True)
    ax.set_ylim((-2, 2))
    ax.set_ylabel('Amplitude')

    ax = fig.add_subplot(2, 1, 2)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.stem(x2, y2) #, 'g.')
    ax.grid(True)
    ax.set_xlabel('Sample Number (Time)')
    ax.set_ylim((-2, 2))
    ax.set_ylabel('Amplitude')

#---------------------------------------------------------------------#

if False:
    fig = matplotlib.pyplot.figure(7)

    n = [ 100, 1000, 10e3, 100e3, 1e6, 5e6 ]
    lt = numpy.array(
        [ 0.000976, 0.007959, 0.078368, 0.820413, 8.163863, 41.388439 ])
    nt = numpy.array(
        [ 0.000458, 0.000946, 0.006032, 0.057410, 0.554600, 2.776934 ])

    ax = fig.add_subplot(2, 1, 1)
    ax.set_title('Relative Performance')
    ax.loglog(n, lt, 'r+-')
    ax.loglog(n, nt, 'g+-')
    ax.set_ylim(-10, 50)
    ax.set_ylabel('seconds')
    ax = fig.add_subplot(2, 1, 2)
    ax.semilogx(n, lt / nt, 'k+-')
    ax.set_ylabel('speedup')
    ax.set_xlabel('N')

#---------------------------------------------------------------------#

matplotlib.pyplot.show()

#--#

# 100:
#<function list_data at 0x74fe0588>: 0.000976 seconds
#<function numpy_data at 0x74fe0540>: 0.000458 seconds

# 1000:
#<function list_data at 0x74f33588>: 0.007959 seconds
#<function numpy_data at 0x74f33540>: 0.000946 seconds

# 10e3
#<function list_data at 0x74ff9588>: 0.078368 seconds
#<function numpy_data at 0x74ff9540>: 0.006032 seconds



# 100e3
#<function list_data at 0x74f9f588>: 0.820413 seconds
#<function numpy_data at 0x74f9f540>: 0.057410 seconds

# 1e6
#<function list_data at 0x74f3b588>: 8.163863 seconds
#<function numpy_data at 0x74f3b540>: 0.554600 seconds

# 5e6:
#<function list_data at 0x74fa6540>: 41.388439 seconds
#<function numpy_data at 0x74fa64f8>: 2.776934 seconds
