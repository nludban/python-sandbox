#!/usr/bin/python3.4

import math

import numpy # as np

import matplotlib.pyplot # as plt

#from matplotlib.lines import Line2D
import matplotlib.markers as markers

#matplotlib.org/examples/lines_bars_and_markers/marker_reference.html
#matplotlib.org/examples/lines_bars_and_markers/line_styles_reference.html

BG = 'lightgray'
FG = 'green'

PI = math.pi
TWO_PI = 2 * math.pi
TWO_PI_J = 2j * math.pi

if True:
    y = [ ]
    for x in range(10):
        y.append(x**2)
    fig = matplotlib.pyplot.figure('First Plot Ever')
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title(r'$y = x^2$')
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(y,
            marker='D', markersize=6, color='blue',
            linestyle='-')
    ax.set_xlabel('X-Axis')
    ax.set_ylabel('Y-Axis')
    ax.set_xlim(-1, 10)
    ax.set_ylim(-1, 85)
    ax.grid(True)

matplotlib.pyplot.show()

#--#
