#!/usr/local/bin/python2.7
#
# A variation on:
#	https://github.com/efloehr/circlemult/blob/master/circlemult.py
#

from __future__ import division

import argparse
import sys

import matplotlib
import matplotlib.colors
import matplotlib.pyplot
import numpy

TWO_PI_J = 2j * numpy.pi

ap = argparse.ArgumentParser(
    description='NumPy powered circlemult.',
    add_help=True)
ap.add_argument('-f', '--freq-1', dest='F1', type=float, default=2.0)
ap.add_argument('-m', '--modulus', dest='N', type=int, default=100)
ap.add_argument('-c', '--color', type=str, default='MediumSeaGreen')
ap.add_argument('-r', '--radius', type=float, default=10)
ap.add_argument('-e', '--freq-0', dest='F0', type=float, default=1.0)
ap.add_argument('-p', '--phase', type=float, default=0.5)
ap.add_argument('-w', '--linewidth', type=float, default=1)

opts = ap.parse_args(sys.argv[1:])

t = numpy.arange(opts.N)
sp = opts.radius * numpy.exp(TWO_PI_J * (opts.phase + (opts.F0 / opts.N) * t))
ep = opts.radius * numpy.exp(TWO_PI_J * (0.5 + (opts.F1 / opts.N) * t))

mix = sp.conj() * ep
#mix = (sp.real + opts.radius) * (ep.real + opts.radius)

amp = numpy.abs(ep - sp)

fig = matplotlib.pyplot.figure(1, figsize=(8, 8))
gs = matplotlib.gridspec.GridSpec(3, 1, height_ratios=[6, 1, 1])

x1, y1 = sp.real, sp.imag
x2, y2 = ep.real, ep.imag
ax = fig.add_subplot(gs[0])
ax.set_title('Multiplication Circle')
#ax.axhline(0, color='lightgray')
#ax.axvline(0, color='lightgray')
lc = matplotlib.collections.LineCollection(
    zip(zip(x1, y1), zip(x2, y2)),
    colors=matplotlib.colors.colorConverter.to_rgba(opts.color),
    linewidths=opts.linewidth)
ax.add_collection(lc)
ax.set_aspect('equal', 'datalim')
ax.margins(0.1)

ax = fig.add_subplot(gs[1])
ax.axhline(0, color='gray')
ax.plot(t, amp, 'b-')
ax.set_ylabel('seglen')

ax = fig.add_subplot(gs[2])
ax.axhline(0, color='gray')
ax.plot(t, mix.real, 'r-')
ax.plot(t, mix.imag, 'g-')
ax.set_ylabel('mixer')

matplotlib.pyplot.show()

#--#
