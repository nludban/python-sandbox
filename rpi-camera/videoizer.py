#!/usr/local/bin/python3.5

import glob
import os
import sys

arg0, output_avi, hostname, x_min, x_max = sys.argv

stills = [ ]
for filename in glob.glob('incoming/%s/*.jpg' % hostname):
    if x_min < filename.split('/')[-1] < x_max:
        stills.append(filename)
with open('stills.txt', 'w') as f:
    f.write('\n'.join(stills))

command = [
    'mencoder',
    '-nosound',
    '-ovc',
    'lavc -lavcopts vcodec=mpeg4:aspect=16/9:vbitrate=8000000',
    #'-vf scale=1920:1080',
    '-vf scale=800:450',
    '-o ' + output_avi,
    '-mf type=jpeg:fps=24',
    'mf://@stills.txt',
    ]

os.system(' '.join(command))

#--#
