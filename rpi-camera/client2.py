#!/usr/local/bin/python3
#
# Adapted from (quirks and all):
#	http://picamera.readthedocs.io/en/release-1.12/recipes2.html
#	4.5. Rapid capture and processing

import io
import math
import os
import socket
import sys
import time

import threading

import requests

import picamera

arg0, server_url, framerate, skiprate = sys.argv
hostname = socket.gethostname()
framerate = int(framerate)
skiprate = int(skiprate)

#python3 client2.py http://servername:8888/upload 24 12
# --> keep 2 fps
# ... 24 240 --> 0.1 Hz

def upload_file(filename, contents):
    r = requests.post(
        server_url,
        data=dict(
            host=hostname,
            ),
        files=dict(
            jpg=(filename, contents, 'image/jpeg' )),
    )
    r.raise_for_status()
    return

#---------------------------------------------------------------------#

# Create a pool of image processors
done = False
lock = threading.Lock()
pool = [ ]

class ImageProcessor(threading.Thread):

    def __init__(self):
        super(ImageProcessor, self).__init__()
        self.stream = io.BytesIO()
        self.filename = None
        self.event = threading.Event()
        self.terminated = False
        self.daemon = True
        self.start()
        return

    def run(self):
        # This method runs in a separate thread
        global done
        while not self.terminated:
            # Wait for an image to be written to the stream
            if self.event.wait(1):
                try:
                    self.stream.seek(0)

                    if self.filename is not None:
                        contents = self.stream.read()
                        upload_file(self.filename, contents)

                    # Set done to True if you want the script to terminate
                    # at some point
                    #done=True	# XXX Never checked???
                finally:
                    # Reset the stream and event
                    self.stream.seek(0)
                    self.stream.truncate()
                    self.filename = None
                    self.event.clear()
                    # Return ourselves to the pool
                    # XXX Not finally :(
                    with lock:
                        pool.append(self)
        return

def streams():
    n = 0
    while not done:
        with lock:
            if pool:
                processor = pool.pop()
            else:
                processor = None
        if processor:
            n += 1
            if n % skiprate == 0:
                # sudo dpkg-reconfigure tzdata
                # But, prefer UTC for long term.
                now = time.time()
                base = time.strftime('%Y%m%d-%H%M%S', time.gmtime(now))
                ext = '%.3f' % (now - math.floor(now))
                processor.filename = '%s_%s.jpg' % ( base, ext[2:] )
            yield processor.stream
            processor.event.set()
        else:
            # When the pool is starved, wait a while for it to refill
            time.sleep(0.1)
    return

with picamera.PiCamera() as camera:
    pool = [ImageProcessor() for i in range(4)]

    camera.hflip = True
    camera.vflip = True

    print('max res= %r' % ( camera.MAX_RESOLUTION, ))

    # 5MP:
    #max res= (2592, 1944) # 4:3 * 648
    # 16:9 * 162 = (2592, 1458)
    #camera.resolution = (640, 480) # 106k/jpg
    #camera.resolution = (2592, 1458) # 1.3-1.6M/jpg
    camera.resolution = (1536, 864) # 16:9 * 96

    # 8MP:
    #max res= PiCameraResolution(width=3280, height=2464)
    # 16:9 * 205 = (3280, 1845)
    # 16:9 * 192 = (3072, 1728)

    #camera.resolution = cam.MAX_RESOLUTION
    #camera.framerate = 0.2 # Hangs.
    camera.framerate = framerate
    print('See monitor for preview; streaming in 2 seconds...')

    #5.15. Preview flickers at full resolution on a V2 module
    #camera.start_preview(resolution=(1024, 768))
    #camera.start_preview(resolution=(1024, 576))

    camera.start_preview()
    time.sleep(2)
    camera.capture_sequence(streams(), use_video_port=True)

# Shut down the processors in an orderly fashion
while pool:
    with lock:
        processor = pool.pop()
    processor.terminated = True
    processor.join()

#--#
