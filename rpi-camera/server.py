#!/usr/local/bin/python3.5

import argparse
import asyncio
import glob
import os
import sys

import aiohttp
import aiohttp.web

#import requests

#---------------------------------------------------------------------#

class IndexView(aiohttp.web.View):

    async def get(self):
        return aiohttp.web.Response(body=b'Hello, world.  %i'
                                    % os.getpid())

class WebView(aiohttp.web.View):

    async def get(self):
        if 'filename' not in self.request.match_info:
            raise aiohttp.web.HTTPFound('/web/viewer.html')
        filename = self.request.match_info['filename']
        f = open('web/{}'.format(filename), 'rb')
        return aiohttp.web.Response(body=f.read())

class UploadView(aiohttp.web.View):

    async def post(self):
        data = await self.request.post()
        hostname = data['host']
        jpg = data['jpg']
        filename = jpg.filename
        fin = jpg.file
        contents = fin.read()
        # XXX date/time_mmm
        with open('incoming/%s/%s' % (
                hostname, filename ), 'wb') as fout:
            fout.write(contents)
        update_manager.notify(hostname, filename)
        return aiohttp.web.Response(body=b'Yay!')

class ImageView(aiohttp.web.View):

    async def get(self):
        hostname = self.request.match_info['hostname']
        filename = self.request.match_info['filename']
        f = open('incoming/%s/%s' % ( hostname, filename ), 'rb')
        return aiohttp.web.Response(body=f.read(),
                                    content_type='image/jpeg')

class UpdateManager:

    def __init__(self):
        self._clients = [ ]
        return

    def _scan(self, known):
        updates = [ ]
        for filename in glob.glob('incoming/*/*.jpg'):
            if filename in known:
                continue
            known.add(filename)
            # (hostname, filename)
            updates.append(tuple(filename.split('/')[1:]))
        return updates

    async def scan_task(self):
        known = set()
        self._scan(known)
        while True:
            await asyncio.sleep(1)
            updates = self._scan(known)
            for hostname, filename in updates:
                self.notify(hostname, filename)
        return

    def attach(self, ws):
        self._clients.append(ws)
        return

    def detach(self, ws):
        self._clients.remove(ws)
        return

    def notify(self, hostname, filename):
        print('Uploaded: /image/%s/%s' % ( hostname, filename ))
        text = bytes(hostname + ':' + filename, 'ascii')
        for c in self._clients:
            c.send_bytes(text)
        return


async def ws_handler(request):

    print('/updates ws_handler:response...')
    ws = aiohttp.web.WebSocketResponse()

    print('/stream ws_handler:prepare...')
    sys.stdout.flush()
    await ws.prepare(request)

    print('/updates ws_handler:loop...')
    sys.stdout.flush()

    
    update_manager.attach(ws)

    async for msg in ws:
        if msg.tp == aiohttp.MsgType.text:
            #print(self.now(), 'Viewer connection read.text:', msg)
            data = bytes(msg.data, 'ascii')
        elif msg.tp == aiohttp.MsgType.binary:
            #print(self.now(), 'Viewer connection read.binary:', msg)
            data = msg.data
        elif msg.tp == aiohttp.MsgType.closed:
            print('Viewer connection closed:', msg)
            break # XXX iterator syntax broken?
        elif msg.tp == aiohttp.MsgType.error:
            print('Viewer connection error:', msg)
            break
        else:
            # ping, pong, close[!d], ???
            print('Viewer connection unknown type:', msg)

    update_manager.detach(ws)

    print('Lost connection to viewer.')

    print('/updates websocket connection closed')
    return ws

#---------------------------------------------------------------------#

if not os.path.isdir('incoming'):
    print('Missing "incoming" directory.  Create and add a\n'
          'subdirectory for each camera client hostname.')
    sys.exit(1)

update_manager = UpdateManager()

evloop = asyncio.get_event_loop()

ap = argparse.ArgumentParser(
    description='Pi web server for monitoring.',
    add_help=False)
ap.add_argument('-h', '--host', dest='host', default='0.0.0.0')
ap.add_argument('-p', '--port', dest='port', type=int, default=8888)
ap.add_argument('-s', '--scan', dest='scan', default=False,
                action='store_true')

opts = ap.parse_args(sys.argv[1:])

app = aiohttp.web.Application()

app.router.add_route('*', '/', IndexView)
app.router.add_route('*', '/web', WebView)
app.router.add_route('*', '/web/', WebView)
app.router.add_route('*', '/web/{filename}', WebView)
app.router.add_route('*', '/upload', UploadView)
app.router.add_route('*', '/updates', ws_handler)
app.router.add_resource('/image/{hostname}/{filename}') \
          .add_route('GET', ImageView)

if opts.scan:
    evloop.create_task(update_manager.scan_task())

# XXX port in use -> connection reset (by ???)
aiohttp.web.run_app(app, host=opts.host, port=opts.port)

#--#
