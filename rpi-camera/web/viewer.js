/*
 * viewer.js
 */

function UpdatesSocket(controller)
{
    var self = { };

    // http://stackoverflow.com/questions/10406930/how-to-construct-a-websocket-uri-relative-to-the-page-uri
    var ipName = window.location.host;

    // ? https://docs.zentri.com/wiconnect/2.3/apps/http_server_ws_simple
    var ws = new WebSocket('ws://' + ipName + '/updates');
    console.log('Continuing websocket setup...');

    ws.onopen = function () {
        console.log('WebSocket open.');
	controller.socket_open();
	return;
    };

    ws.onerror = function (error) {
        console.error('WebSocket error: ', error);
	controller.socket_error();
	ws.close();	// ???
    };

    ws.onclose = function (closeEvent) {
        console.log('WebSocket connection has been closed!'
		    + ' (code=' + closeEvent.code
		    + ', reason=' + closeEvent.reason
		    + ', clean=' + closeEvent.wasClean
		    + ')');
	controller.socket_close();
	return;
    };

    //self._reader = null;

    function file_onload(event) {
	var reader = event.target;
        var text = reader.result;
	//console.log('onload(' + text + ')');
	controller.socket_message(text);
	return;
    };

    ws.onmessage = function (event) {
	//http://stackoverflow.com/questions/9462879/how-do-i-tell-the-type-of-websocket-onmessages-parameter
        if (event.data instanceof Blob) {
            var reader = new FileReader();
            reader.onload = file_onload;
            reader.readAsText(event.data);
        }
	return;
    };

    self.send = function (text) {
	ws.send(text);
	return;
    };

    self.close = function () {
	ws.close();
	return;
    }

    console.log('Completed websocket setup.');
    return self;
};


function ImageLoader(title_elm, image_elm, src)
{
    jQuery(title_elm).text(src);
    var canvas = jQuery(image_elm)[0];
    var ctx = canvas.getContext('2d');
    var i = new Image();
    i.onload = function () {
	ctx.drawImage(i, 0, 0, 800, 450);
    };
    i.src = src;
    return;
};


function UpdatesController()
{
    self = { };

    self._sock = UpdatesSocket(self);

    self.socket_open = function () { };

    self.socket_error = function () { };

    self.socket_close = function () { };

    self.socket_message = function (text) {
	text = text.split(/:/g)
	var hostname = text[0];
	var filename = text[1];
	ImageLoader('#t-' + hostname,
		    '#' + hostname,
		    '/image/' + hostname + '/' + filename);
	return;
    };

    return self;
};

function init_globals()
{
    controller = UpdatesController();

    return;
}

jQuery(document).ready(init_globals);


/**/
