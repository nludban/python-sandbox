#!/usr/bin/env python

from __future__ import division

import math
import time

from sense_hat import SenseHat

import Tkinter as tk

BLK = ( 0, 0, 0 )
RED = ( 255, 0, 0 )
GRN = ( 0, 255, 0 )
BLU = ( 0, 0, 255 )
WHT = ( 255, 255, 255 )

class BarGraph(object):

    def __init__(self, hat, column, scale=1.0):
        self._hat = hat
        self._column = column
        self._scale = scale
        return

    def set(self, p):
        p /= self._scale
        p *= 8
        c = [ BLU, ] * 8
        n = int(abs(p))
        h = ((abs(p) - n) >= 0.5)
        if (p > 0):
            #c[:n] = (GRN,)
            for k in range(min(n, 7)):
                c[k] = GRN
        if (p < 0):
            #c[-n:] = (RED,)
            for k in range(max(-8, -n), 0):
                c[k] = RED
        x = self._column
        for y, rgb in zip(range(8), c):
            self._hat.set_pixel(x, y, rgb)
        return


class BarPlot(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self._xval = tk.StringVar()
        self._yval = tk.StringVar()
        self._zval = tk.StringVar()
        scale_opts = dict(
            length=150,
            from_=-1.2, to=+1.2,
            #tickinterval=0.2,
            showvalue=False,
            orient=tk.HORIZONTAL,
            digits=3, resolution=0.01)

        x_label = tk.Label(text='X')
        x_label.grid(row=1, column=1)
        x_text = tk.Entry(textvariable=self._xval, justify=tk.RIGHT)
        x_text.grid(row=1, column=2)
        self._xscale = tk.Scale(variable=self._xval, **scale_opts)
        self._xscale.grid(row=1, column=3)

        y_label = tk.Label(text='Y')
        y_label.grid(row=2, column=1)
        y_text = tk.Entry(textvariable=self._yval, justify=tk.RIGHT)
        y_text.grid(row=2, column=2)
        self._yscale = tk.Scale(variable=self._yval, **scale_opts)
        self._yscale.grid(row=2, column=3)

        z_label = tk.Label(text='Z')
        z_label.grid(row=3, column=1)
        z_text = tk.Entry(textvariable=self._zval, justify=tk.RIGHT)
        z_text.grid(row=3, column=2)
        self._zscale = tk.Scale(variable=self._zval, **scale_opts)
        self._zscale.grid(row=3, column=3)

        self._plot = tk.Canvas(height=200, width=400)
        self._plot.grid(row=4, column=1, columnspan=3)
        for y in ( 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4 ):
            kwargs = { }
            if y == 1.0:
                kwargs['width'] = 2
            y = 100 - 200 * (y - 1.0)
            self._plot.create_line(0, y, 400, y, **kwargs)
        self._t = [ x * 8 for x in range(50) ]
        self._z = [ 1.0 ] * 50
        self._trace = self._create_trace(self._t, self._z)
        return

    def _create_trace(self, t, z):
        args = [ None ] * (len(t) * 2)
        for k in range(len(t)):
            args[k*2+0] = t[k]
            args[k*2+1] = 100 - 200 * (z[k] - 1.0)
        return self._plot.create_line(*args)

    def update(self, x, y, z):
        self._xval.set('%.2f' % x)
        self._yval.set('%.2f' % y)
        self._zval.set('%.2f' % z)
        del self._z[0]
        self._z.append(z)
        self._plot.delete(self._trace)
        self._trace = self._create_trace(self._t, self._z)
        return


def main(hat):
    delay_ms = 50
    plot = BarPlot()
    plot.master.title('Gravity Test')
    hat.clear()
    scale = 1.2
    bgx = BarGraph(hat, 1, scale)
    bgy = BarGraph(hat, 3, scale)
    bgz = BarGraph(hat, 5, scale)
    def update():
        a = hat.get_accelerometer_raw()
        x, y, z = a['x'], a['y'], a['z']
        bgx.set(x)
        bgy.set(y)
        bgz.set(z)
        plot.update(x, y, z)
        plot.after(delay_ms, update)
        return
    plot.after(500, update)
    plot.mainloop()
    return


def demo(hat):
    hat.set_pixel(0, 0, RED)	# USB corner
    hat.set_pixel(7, 0, GRN)	# ETH corner
    hat.set_pixel(7, 7, BLU)	# POW corner
    hat.set_pixel(0, 7, WHT)	# LED corner
    for c in ( RED, GRN, BLU, WHT, BLK ):
        time.sleep(3)
        for x in range(8):
            for y in range(8):
                hat.set_pixel(x, y, c)
    return

if (__name__ == '__main__'):
    hat = SenseHat()
    hat.set_rotation(90)
    hat.clear()
    try:
        #demo(hat)
        main(hat)
    except KeyboardInterrupt:
        pass
    hat.clear()

#--#
